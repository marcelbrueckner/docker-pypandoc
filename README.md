# pypandoc

A Docker image with [Python](https://www.python.org/) and [pandoc](https://pandoc.org/).

Doesn't actually contain the pypandoc module as this image is meant to be used as the build image for [this repository's](https://gitlab.com/marcelbrueckner/adventofcode) CI pipeline where all Python dependencies are installed via `pip`.
