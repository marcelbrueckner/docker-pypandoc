FROM pandoc/core:2.8.1 AS pandoc

FROM python:3.8-alpine3.10
LABEL maintainer='Marcel Brückner'

ARG USER=pypandoc
ARG UID=1000
ARG GID=1000
ENV HOME /home/${USER}

RUN addgroup --gid "$GID" "$USER" \
      && adduser \
      --disabled-password \
      --gecos "" \
      --ingroup "$USER" \
      --uid "$UID" \
      "$USER"

RUN apk add --no-cache \
      git \
      gmp \
      libffi \
      lua5.3 \
      lua5.3-lpeg

COPY --from=pandoc /usr/bin/pandoc* /usr/bin/

USER ${USER}
WORKDIR ${HOME}